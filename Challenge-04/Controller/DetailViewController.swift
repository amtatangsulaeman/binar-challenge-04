//
//  DetailViewController.swift
//  Challenge-04
//
//  Created by Tatang Sulaeman on 11/04/22.
//

import UIKit

class DetailViewController: UIViewController {
    var imageDetailCar: String?
    var detailNameCar: String?
    var detailPriceCar: String?

    
    @IBOutlet weak var imageCar: UIImageView!
    @IBOutlet weak var nameCar: UILabel!
    @IBOutlet weak var priceCar: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageCar.loadImage(resource: imageDetailCar)
        nameCar.text = "\((detailNameCar)!)"
        priceCar.text = "\((detailPriceCar)!)"

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
