//
//  CarListViewController.swift
//  Challenge-04
//
//  Created by Tatang Sulaeman on 08/04/22.
//

import UIKit

class CarListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var displayedCar: [Car] = Car.carList()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CarListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let carCount: Int = displayedCar.count
        return carCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarListTableViewCell", for: indexPath) as! CarListTableViewCell
        
        let row: Int = indexPath.row
        let car: Car = displayedCar[row]
        cell.fill(with: car)
        return cell
    }
    

    
    
}
