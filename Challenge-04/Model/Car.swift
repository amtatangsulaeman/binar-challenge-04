//
//  Car.swift
//  Challenge-04
//
//  Created by Tatang Sulaeman on 08/04/22.
//

import Foundation

struct Car: Decodable {
    let name: String
    let price: String
    let image: String
}

extension Car {
    static func carList() -> [Car] {
        let json =
                """
                [
                    {
                        "name": "Brio",
                        "price": "300000",
                        "image": "https://imgcdnblog.carbay.com/wp-content/uploads/2021/01/19112321/Honda-Brio-Satya.jpg"
            
                    },
                    {
                        "name": "Jazz",
                        "price": "500000",
                        "image": "https://imgx.gridoto.com/crop/132x132:1156x813/700x500/filters:watermark(file/2017/gridoto/img/watermark_otomotifnet.png,5,5,60)/photo/2021/03/16/honda-jazz-1jpeg-20210316071409.jpeg"
                    },
                    {
                        "name": "Xpander",
                        "price": "500000",
                        "image": "https://imgcdnblog.carmudi.com.ph/wp-content/uploads/2020/08/21154445/ZW-Mitsubishi-Xpander_01.jpg"
                    },
                    {
                        "name": "Mobilio",
                        "price": "400000",
                        "image": "https://imgcdnblog.carbay.com/wp-content/uploads/2021/01/13072319/OTO-Honda-Mobilio-RS.jpg"
            }
                    

                ]
            """
        do {
            let jsonData = Data(json.utf8)
            let cars: [Car] = try JSONDecoder().decode([Car].self, from: jsonData)
            return cars
        } catch {
            print(String(describing: error))
            return []
        }
    }
}
