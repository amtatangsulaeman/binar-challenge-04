//
//  HomeTableViewCell.swift
//  Challenge-04
//
//  Created by Tatang Sulaeman on 08/04/22.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var imageCar: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fill(with car: Car) {
        imageCar.loadImage(resource: car.image)
        nameLabel.text = car.name
        priceLabel.text = String(car.price)
    }
    
}
